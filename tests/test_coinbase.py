import unittest

import sys
sys.path.append('../')
from traderbot.setup.load_prefs import load_prefs


class TestCoinbase(unittest.TestCase):

    def test_json_api_object(self):

        broker, _ = load_prefs()
        self.assertEqual(type(broker).__name__, 'Coinbase')

        price = broker.get_price()
        self.assertEqual(type(price), float)



if __name__ == '__main__':
    unittest.main()
