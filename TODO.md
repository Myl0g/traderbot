# To-Do

This file lists anything that needs to get done in this project.

* Issue [#12](https://github.com/Myl0g/traderbot/issues/12)
* change limits from set dollar amounts to percent changes in value
* write unit tests
* add other exchanges (e.g. Kraken)
