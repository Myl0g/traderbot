"""A Coinbase account, using API keys (Oauth not supported)"""

from coinbase.wallet.client import Client
from traderbot.exchanges.broker import Broker
import json

class Coinbase(Broker):
    """A Coinbase account, using API keys (Oauth not supported)"""

    def __init__(self, currency, api_key, api_secret):
        Broker.__init__(self, currency)

        self.broker_client = Client(api_key, api_secret)  # Used to get prices
        self.account = self.broker_client.get_primary_account()  # Used to perform actual transactions

    def buy(self, amount):

        self.account.buy(amount=amount, currency=self.currency)

    def sell(self, amount):

        self.account.sell(amount=amount, currency=self.currency)

    def get_price(self):

        price_json = self.broker_client.get_sell_price(currency_pair=self.currency + '-USD')
        price_dict = eval(json.dumps(price_json))
        return float(price_dict["amount"])


    def fiat_to_crypto(self, amount, fiat="USD"):

        return self.get_price() / amount

    def crypto_to_fiat(self, amount, fiat="USD"):

        return amount * self.get_price()
