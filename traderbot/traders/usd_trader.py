"""Simulates a trader, using a Broker to interact with the market."""

class USD_Trader:
    """Simulates a trader, using a Broker to interact with the market through USD and crypto."""

    def __init__(self, usd_per_buy, minimum_price, maximum_price, broker):
        self.usd_per_buy = usd_per_buy
        self.min = minimum_price
        self.max = maximum_price
        self.broker = broker

        self.total_profit = 0
        self.just_bought = []  # Amount to be sold
        self.just_bought_usd = []

    def past_min(self):
        """Checks if the price has hit or gone under the minimum value."""

        price = self.broker.get_price()

        return price <= self.min

    def past_max(self):
        """Checks if the price has hit or gone over the maximum value."""

        price = self.broker.get_price()

        return price >= self.max

    def buy(self):
        """Uses the broker to buy self.usd_per_buy of crypto."""

        crypto_per_buy = self.broker.fiat_to_crypto(self.usd_per_buy)

        self.broker.buy(crypto_per_buy)  # Buys amount of crypto worth self.usd_per_buy

        self.just_bought += crypto_per_buy
        self.just_bought_usd += self.broker.crypto_to_fiat(self.just_bought)

    def sell(self):
        """Uses the broker to sell all previously purchased crypto."""

        for purchase in range(len(self.just_bought)):
            self.broker.sell(self.just_bought[purchase])  # Sell previously bought amount
            self.total_profit += (self.just_bought_usd[purchase] - self.usd_per_buy)  # Get profit

        self.just_bought = []
        self.just_bought_usd = []
