"""Performs initial configuration based on user input."""

from homedir import get_home_dir


def get_and_write(input_request, file, key):
    """Gets user input and immediately write result to file."""

    result = input(input_request + " ")
    file.write(key + "=" + result + "\n")


def choose_currency():
    """Gets user input on their preferred currency."""

    print("At the moment, we only support trading a single currency at a time.")
    print("Here are your choices:")

    print("[1] Bitcoin")
    print("[2] Ethereum")
    print("[3] Litecoin")

    while True:

        currency = input("Which do you choose? ")

        if currency == "1":

            return "Currency=BTC\n"

        elif currency == "2":

            return "Currency=ETH\n"

        elif currency == "3":

            return "Currency=LTC\n"

        else:

            print("Please enter in a valid number.")


def first_time_setup():
    """Performs initial configuration based on user input."""

    print("Looks like this is your first time using trader.py!")
    prefs = open(get_home_dir() + ".trader-prefs", 'x')

    print("\nAt the moment, we only accept Coinbase as an exchange.")
    prefs.write("Broker=Coinbase\n")

    # API Keys
    print("\nPlease follow the instructions under the wiki to generate an API key for Coinbase.")

    get_and_write("Enter that key here.", prefs, "Key")
    get_and_write("Enter your secret key.", prefs, "Secret")

    print("\nPlease note that it takes 48 hours for an API key to activate.\n")

    # Currency
    prefs.write(choose_currency())

    # USD Per Buy
    get_and_write("How much (in USD) would you like to buy at a time?", prefs, "USD_PER_BUY")

    # Low
    get_and_write("What low value would you like to trigger a buy? (USD)", prefs, "Low")

    # High
    get_and_write("What high value would you like to trigger a sell? (USD)", prefs, "High")

    prefs.close()
    print("----------------")
    print("Check if your values are OK in the .trader-prefs file in the home folder.\nEdit if necessary.")
    final_confirm = input("Then, enter 'y' ")

    valid_confirm = False
    while not valid_confirm:
        if final_confirm == "y":
            valid_confirm = True
        else:
            print("'y' not entered.")
            final_confirm = input("Please enter 'y'. ")
