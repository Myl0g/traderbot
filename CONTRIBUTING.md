# Contributing

Thanks for taking an interest in helping to make a better traderbot.

If you've seen a problem (such as an uncaught exception, unexpected behavior, etc.) then take a moment to open an [Issue](https://gitlab.com/Myl0g/traderbot/issues/new).

We also accept [Merge Requests](https://gitlab.com/Myl0g/traderbot/merge_requests) if you'd like to add a feature or make some other contribution yourself.

Here's a link to this project's [To-Do](TODO.md) list if you need inspiration.

Thanks for taking the time to look this page over, and happy trading!
